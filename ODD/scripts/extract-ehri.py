"""
PREAMBLE
- Description: Retrieves all elements and their associated attributes in the corpus.
- Author: Sarah Bénière
- Date: May 2023
- Input: XML files
- Output: CSV file
- Running instructions:
    ======
    python script.py arg1
    arg1: absolute path for the corpus
    ======
"""

import os
import re
import sys
from bs4 import BeautifulSoup

def extraction():
    for root, dirs, files in os.walk(sys.argv[1]):
        for filename in files:
            with open(os.path.join(root, filename), 'r', encoding='utf-8') as xml_file:
                xml_content = xml_file.read()
                print(f"Reading: {filename}")
                
                soup = BeautifulSoup(xml_content, 'xml')
                


extraction()