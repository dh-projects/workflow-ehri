"""
PREAMBLE
- Description: Produces the list of the files in which the specified element appears.
- Author: Sarah Bénière
- Date: April 2023
- Usage:
    ======
    python name_of_the_script.py arg1
    arg1: absolute path for the corpus
"""

import os
import sys
from bs4 import BeautifulSoup

files_containing_tag = set()

for root, dirs, files in os.walk(sys.argv[1]):
    for filename in files:
        with open(os.path.join(root, filename), 'r', encoding='UTF-8') as xml_file:
                xml_content = xml_file.read()
                soup = BeautifulSoup(xml_content, 'xml')
                
                for tag in soup.find_all():
                    if tag.name == "biblScope":  # element name here
                        files_containing_tag.add(filename)                        

for filename in files_containing_tag:
    print(filename)