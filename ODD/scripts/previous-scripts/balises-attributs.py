#Récupère les balises XML-TEI utilisées par le projet EHRI

import os #permet de naviguer dans le dossier contenant tous les fichiers
import re #permet d'utiliser les expressions régulières
import sys
from bs4 import BeautifulSoup #importe la bibliothèque BeautifulSoup

regex_pattern = 'EHRI-.*\.xml' #regex qui permet d'ouvrir tous fichiers dont le nom commence par EHRI- et se termine par .xml

for root, dirs, files in os.walk(sys.argv[1]): #se balade à l'intérieur du dossier racine
    for filename in files: #pour chaque nom de fichier
        if re.match(regex_pattern, filename): #si le nom du fichier match la regex définie dans regex_pattern    
            with open(os.path.join(root, filename), 'r', encoding='UTF-8') as f: #ouvre les fichiers de corpus_ehri en mode lecture et stocke le fichier ouvert dans la variable f / os.path.join() permet d'utiliser le bon séparateur de chemin en fonction du système d'exploitation
                xml_content = f.read() #la variable xml_content stocke le contenu lu depuis le fichier ouvert dans f avec la méthode read()
                print("Fichier en lecture : " + filename)

soup = BeautifulSoup(xml_content, 'xml') #parse le document avec BeautifulSoup(fichier parsé, 'parser XML intégré à BeautifulSoup')
sans_doublons = set() #définit la variable sans_doublons qui est un set()
for tag in soup.find_all(True): #attrape toute les balises
    sans_doublons.add(tag.name) #ajoute le nom de chaque balise dans sans_doublons
    attrs = tag.attrs #crée un dictionnaire attrs (attributs) qui stocke les noms des attributs des bailses et leurs valeurs dans une paire
    for attr in attrs:
        sans_doublons.add(f"{attr}='{attrs[attr]}'")
print(sans_doublons) #renvoie le nom des balises

#Maintenant on veut compter le nombre d'occurrences de chaque balise et attributs (pour observer les tendances)
#Et exporter les résultats dans un document CSV