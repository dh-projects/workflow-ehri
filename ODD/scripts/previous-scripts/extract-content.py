"""
PREAMBLE
- Description: Prints the content of the specified element.
- Author: Sarah Bénière
- Date: May 2023
- Usage:
	======
	python name_of_the_script.py arg1
	arg1: absolute path for the corpus
"""

import os
import sys
from bs4 import BeautifulSoup

for root, dirs, files in os.walk(sys.argv[1]):
	for filename in files:
		with open(os.path.join(root, filename), 'r', encoding = 'UTF-8') as xml_file:
			xml_content = xml_file.read()
			soup = BeautifulSoup(xml_content, 'xml')

			for tag in soup.find_all():
				if tag.name == "keywords":
					print(f"{filename} : {tag}")