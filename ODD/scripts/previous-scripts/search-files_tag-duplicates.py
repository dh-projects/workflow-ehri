"""
PREAMBLE
- Description: Produces the list of the files in which the specified element appears more than once.
- Author: Sarah Bénière
- Date: April 2023
- Usage:
    ======
    python name_of_the_script.py arg1
    arg1: absolute path for the corpus
"""

import os
import sys
from bs4 import BeautifulSoup

def search_duplicates():
	for root, dirs, files in os.walk(sys.argv[1]):
		for filename in files:
			with open(os.path.join(root, filename), 'r', encoding='UTF-8') as xml_file:
				xml_content = xml_file.read()
				soup = BeautifulSoup(xml_content, 'xml')
				occurrences = 0  # initiates an "occurrences" variable with a value of 0

				for tag in soup.find_all():
					if tag.name == "p":  # element name here
						occurrences += 1  # adds 1 to "occurrences" every time the tag is encountered
				if occurrences > 1:
					print(filename)