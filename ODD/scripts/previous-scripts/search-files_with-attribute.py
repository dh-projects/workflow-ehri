"""
PREAMBLE
- Description: Produces the list of files in which the specified attribute appears.
- Author: Sarah Bénière
- Date: April 2023
- Usage:
    ======
    python name_of_the_script.py arg1
    arg1: absolute path for the corpus
"""

import os
import sys
from bs4 import BeautifulSoup

for root, dirs, files in os.walk(sys.argv[1]):
	for filename in files:
		with open(os.path.join(root, filename), 'r', encoding='UTF-8') as xml_file:
			xml_content = xml_file.read()
			soup = BeautifulSoup(xml_content, 'xml')

			files_with_attribute = soup.find_all(attrs={"rendition": True})  # attribute name here
			if len(files_with_attribute) > 0: 
				print(filename)