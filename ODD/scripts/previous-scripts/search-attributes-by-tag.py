"""
PREAMBLE
- Description: Produces the list of attributes contained by the specified element.
- Author: Sarah Bénière
- Date: April 2023
- Usage:
    ======
    python name_of_the_script.py arg1
    arg1: absolute path for the corpus
"""

import os
import sys
from bs4 import BeautifulSoup

attributes = set()

for root, dirs, files in os.walk(sys.argv[1]):
    for filename in files:
        with open(os.path.join(root, filename), 'r', encoding='UTF-8') as xml_file:
                xml_content = xml_file.read()
                soup = BeautifulSoup(xml_content, 'xml')

                for tag in soup.find_all():
                    if tag.name == "title":  # element name here
                        for attribute in tag.attrs:
                             attributes.add(attribute)

print (attributes)