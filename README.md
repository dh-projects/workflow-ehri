# Workflow-EHRI
📝 last update: 2023-09-03

## 💡 Presentation of the Project
The [European Holocaust Research Infrastructure](https://www.ehri-project.eu/) (EHRI) is a transnational organization with partners all across Europe, Israel, and the United States of America. They promote **collaboration on Holocaust research** and easy access to scattered sources. To this end, they created the [EHRI Online Editions](https://www.ehri-project.eu/ehri-online-editions), which are **collections of archival documents** on the Holocaust, gathered around a more specific theme like testimonies on the persecution of Jews or diplomatic reports for instance.  

## 🧱 Structure of the Repository
* [Encoding](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/Encoding)
	* [Templates-Index](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/Encoding/Templates-Index)
		* [Organization entry](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/Templates-Index/organization-index.xml)
		* [Person entry](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/Templates-Index/person-index.xml)
		* [Place entry](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/Templates-Index/place-index.xml)
		* [Term entry](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/Templates-Index/term-index.xml)
	* [Structure of EHRI files](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/template-ehri-files.xml)

* [ODD](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/ODD)
	* ODD ([`.xml`](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/ODD-EHRI.xml) – [`.html`](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/ODD-EHRI.html) – [`.pdf`](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/ODD-EHRI.pdf))
	* [RelaxNG Schema](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/out/ODD-EHRI.rng)
	* [Overview Table](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/overview-table.pdf)
	* [Python scripts](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/ODD/scripts)  

* [TEI-Publisher](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/TEI-Publisher)
	* [Visual mock-ups for the application](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/TEI-Publisher/EHRI-app-mockup.pdf)
	* [Specifications](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/TEI-Publisher/Specifications.pdf)

* [Presentation slides](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/2023-07-04_EHRI-Meeting.pdf) from the EHRI meeting (4 July 2023)  
* [Report on the EHRI Online Editions](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/EHRI-Report.pdf)
  
## 📋 Content
This repository holds the workflow for the semi-automated processing of EHRI's digital editions. The repository is composed of three folders, one for each [issue](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/issues).  

### ✅ Issue no. 1 — Writing an exhaustive ODD
The "[ODD](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/ODD)" folder contains all the files related to the creation of the exhaustive ODD for the EHRI digital editions.
* The [overview table](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/overview-table.pdf) summarizes the elements and attributes used by the editors for the four existing digital editions ([BeGrenzte Flucht](https://begrenzte-flucht.ehri-project.eu/), [Early Holocaust Testimony](https://early-testimony.ehri-project.eu/), [Diplomatic Reports](https://diplomatic-reports.ehri-project.eu/), and [Nisko](https://nisko-transports.ehri-project.eu/)). This is the result of the preparatory work for writing the actual ODD.
* The [Python scripts](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/ODD/scripts) (extraction and research) and their [results](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/ODD/scripts/results/results_ordered_by_key), used as a basis for the overview table.
* The [updated version of the ODD](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/ODD-EHRI.xml) (**_IN PROGRESS_**), available in different formats (XML, HTML, and PDF).
* The RelaxNG [schema](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/ODD/out/ODD-EHRI.rng) that should be applied to the EHRI files to ensure conformance to the ODD.
  
### 🔜 Issue no. 2 — Creating a semi-automated processing workflow
The "[Encoding](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/Encoding)" folder will contain:
* The encoding templates for [metadata](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/Encoding/template-ehri-files.xml) and [index entries](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/Encoding/Templates-Index).
* General guidelines for the semantic encoding of the files, and a tutorial for adding a named entity in one of the indices.
* The Python script for an automated encoding of the files' `<teiHeader>` (metadata), with an exhaustive step-by-step guide to run the script.
  
### 🔜 Issue no. 3 — Setting up a dedicated TEI Publisher application
The "[TEI-Publisher](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/tree/main/TEI-Publisher)" folder will contain:
* [Specifications for the application](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/TEI-Publisher/Specifications.pdf)
* [Visual mock-ups for the application](https://gitlab.inria.fr/dh-projects/workflow-ehri/-/blob/main/TEI-Publisher/EHRI-app-mockup.pdf)
* TEI Publisher prototype application

## 📑 Communications
* "Writing an ODD for the EHRI Online Editions -- Preparatory Work." _Digital Intellectuals_, 5 June 2023, https://digitalintellectuals.hypotheses.org/4765.
* "Writing an ODD for the EHRI Online Editions -- Specifications and Documentation." _Digital Intellectuals_, 28 July 2023, https://digitalintellectuals.hypotheses.org/5034.